# -*- mode: ruby -*-
# vi: set ft=ruby :

# Set IP address variable
ip_addr = "10.0.0."

Vagrant.configure("2") do |config|

    # OS box (image)
    config.vm.box = "ubuntu/xenial64"

    # Virtualization provider
    config.vm.provider "virtualbox"

    # Master host provisioning
    $ansible_bootsrap = <<-SCRIPT
      add-apt-repository ppa:ansible/ansible -y
      apt-get update
      apt-get install python-minimal unzip ansible -y
    SCRIPT

    # Mitogen plugin installation
    $ansible_mitogen = <<-SCRIPT
      mkdir /etc/ansible/strategies
      wget https://networkgenomics.com/try/mitogen-0.2.7.tar.gz -P /tmp
      tar -zxvf /tmp/mitogen-0.2.7.tar.gz -C /etc/ansible/strategies
      sed -i 's/#strategy_plugins/strategy_plugins/' /etc/ansible/ansible.cfg
      sed -i 's+strategy_plugins.*+strategy_plugins = /etc/ansible/strategies/mitogen-0.2.7/ansible_mitogen/plugins/strategy+' /etc/ansible/ansible.cfg
      sed -i 's/#strategy/strategy/' /etc/ansible/ansible.cfg
      sed -i 's/strategy =.*/strategy = mitogen_linear/' /etc/ansible/ansible.cfg
      rm -rf /tmp/mitogen-0.2.7.tar.gz
    SCRIPT

    # Set Ansible host_key_checking to false (DO NOT DO IT AT HOME!)
    $ansible_key_checking = <<-SCRIPT
      sed -i 's/#host_key_checking/host_key_checking/' /etc/ansible/ansible.cfg
    SCRIPT

    # Add hosts to /etc/hosts file
    $hosts = <<-SCRIPT
      printf '\n\n#{ip_addr}10 ansible-master\n' >> /etc/hosts
      printf '#{ip_addr}1 node1\n' >> /etc/hosts
      printf '#{ip_addr}2 node2\n' >> /etc/hosts
      printf '#{ip_addr}3 node3\n >> /etc/hosts
    SCRIPT

    # Ansible inventory creation
    $ansible_hosts = <<-SCRIPT
      truncate -s 0 /etc/ansible/hosts
      printf '[cluster:children]\nmaster\nslave\n\n' >> /etc/ansible/hosts
      printf '[master]\nansible-master\n\n' >> /etc/ansible/hosts
      printf '[slave]\nnode1\nnode2\nnode3' >> /etc/ansible/hosts
    SCRIPT

    # SSH key generation
    $ssh_key_generation = <<-SCRIPT
      ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
      cp /home/vagrant/.ssh/id_rsa.pub /home/vagrant/vagrant/ansible.pub
    SCRIPT

    # Ansible nodes provisioning
    $node_bootstrap = <<-SCRIPT
      apt-get update
      apt-get install unzip python-minimal -y
    SCRIPT

    $ssh_key_insert = <<-SCRIPT
      cat /home/vagrant/vagrant/ansible.pub >> /home/vagrant/.ssh/authorized_keys
    SCRIPT

    # Insert your existing pubkey to ansible nodes
    config.ssh.insert_key = false
    config.ssh.private_key_path = ['~/.vagrant.d/insecure_private_key', '~/.ssh/szymkey']
    config.vm.provision "file", source: "~/.ssh/szymkey.pub", destination: "~/.ssh/authorized_keys"

    # Mount current folder for key sharing purposes
    config.vm.synced_folder ".", "/home/vagrant/vagrant"

    # Define ansible mater host
    config.vm.define "ansible" do |ansible|
      ansible.vm.hostname = "ansible"
      ansible.vm.network "private_network", ip: "#{ip_addr}10"
      ansible.vm.provision "shell", inline: $ansible_bootsrap
      ansible.vm.provision "shell", inline: $ansible_key_checking
      ansible.vm.provision "shell", inline: $hosts
      ansible.vm.provision "shell", inline: $ansible_hosts
      ansible.vm.provision "shell", inline: $ansible_mitogen
      ansible.vm.provision "shell", inline: $ssh_key_generation, privileged: false
    end

    # Define multiple hosts at once (IP addr 10.0.0.1-10.0.0.2)
    (1..3).each do |i|
      config.vm.define "node#{i}" do |node|
        node.vm.hostname = "ubuntu#{i}"
        node.vm.network "private_network", ip: "#{ip_addr}#{i}"
        node.vm.provision "shell", inline: $node_bootstrap
        node.vm.provision "shell", inline: $ssh_key_insert, privileged: false
      end
    end
  end

